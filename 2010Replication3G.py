import matplotlib.pyplot as plt
import numpy as np
import math
import time
import random
import csv


def decision(probability):
    return random.random() < probability


Group_simulations=True
plots_visualization = False
arousal_circular = False

delay = 0.00 #Timestep velocity when plot visualization is false
num_simulations = 50


timestep = 0
num_episodes = 70000


#ARENA SIZE
x_min=0
y_min=0
x_max=400
y_max=400


# Initial state in x, y and z
robot_x=random.randint(10,390)
robot_y=random.randint(10,390)

robot_z = 0

#orientation variability
mu = 0 #mean
sigma = 0.15 #DV

#Probabilistic switchin mechanism
security_p_max = 0.05
security_p_min = 0.002
arousal_p_max = 0.5
arousal_p_min = 0.0005
food_p_max = 0.1
food_p_min = 1


security_low_DV = 0
security_high_DV = 0.833
arousal_low_DV = 0.166
arousal_high_DV = 0.9
food_low_DV = 0.166
food_high_DV = 0.9


th_switcher_arousal= 0.05
th_switcher_security = 0.05
th_switcher_food=0.05


#------------------ dVs and weighting factors---------
dV_arousal = arousal_low_DV
arousal_k = 0.9
weighting_factor_arousal = arousal_k
dV_security = security_low_DV
security_k = 0.7
weighting_factor_security = security_k
dV_food = food_low_DV
food_k = 0.4
weighting_factor_food = food_k


# CSV data list
Xposition_list = []
Yposition_list = []
aVarousal_list = []
dVarousal_list = []
aVsecurity_list = []
dVsecurity_list = []
aVfood_list = []
dVfood_list = []



#FUNCTION TO CALCULATE INTENSITY WITH QUARTIC KERNEL
def kde_quartic(d,h):
        dn=d/h
        P=(15/16)*(1-dn**2)**2
        return P

#-------------------AROUSAL GRADIENT-----------------
#POINT DATASET
if arousal_circular == True:
    x=[x_max / 2]
    y=[y_max / 2]
else:
    step=10

    bot_left_x=round(x_max/5)
    bot_left_y=round(y_max/5)

    bot_right_x=x_max-bot_left_x
    bot_right_y=bot_left_y

    mid_left_x=bot_left_x + step
    mid_left_y=round(y_max/2)

    x=[]
    y=[]
    for i in range(25):
        if i == 6 or i == 8 or i ==10 or i == 11 or i == 13 or i ==14 or i==16 or i==18:
            pass
        else:
            x.append(bot_left_x)
            y.append(bot_left_y + (i*step))

    for i in range(25):
        if i == 6 or i == 8 or i ==10 or i == 11 or i == 13 or i ==14 or i==16 or i==18:
            pass
        else:
            x.append(bot_right_x)
            y.append(bot_right_y + (i*step))

    for i in range(23):
        if i ==0 or i == 2 or i == 5 or i == 7 or i==11 or i ==15 or i==17  or i ==20 or i==22:
            pass
        else:
            x.append(mid_left_x + (i*step))
            y.append(mid_left_y)



#DEFINE GRID SIZE AND RADIUS(h)
grid_size=1
if arousal_circular == True:
    h=200
else:
    h=100



#CONSTRUCT GRID
x_grid=np.arange(x_min,x_max,grid_size)
y_grid=np.arange(y_min,y_max,grid_size)
x_mesh_arousal,y_mesh_arousal=np.meshgrid(x_grid,y_grid)

#GRID CENTER POINT
xc=x_mesh_arousal+(grid_size/2)
yc=y_mesh_arousal+(grid_size/2)


#PROCESSING
arousal_intensity_list=[]
for j in range(len(xc)):
    intensity_row=[]
    for k in range(len(xc[0])):
        kde_value_list=[]
        for i in range(len(x)):
            #CALCULATE DISTANCE
            d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
            if d<=h:
                p=kde_quartic(d,h)
            else:
                p=0
            kde_value_list.append(p)
        #SUM ALL INTENSITY VALUE
        p_total=sum(kde_value_list)
        intensity_row.append(p_total)
    arousal_intensity_list.append(intensity_row)




 #-------------------SECURITY GRADIENT-----------------
#POINT DATASET
x=[bot_left_x]
y=[bot_left_y]

#DEFINE GRID SIZE AND RADIUS(h)
grid_size=1
h=70

#CONSTRUCT GRID
x_grid=np.arange(x_min,x_max,grid_size)
y_grid=np.arange(y_min,y_max,grid_size)
x_mesh_security,y_mesh_security=np.meshgrid(x_grid,y_grid)

#GRID CENTER POINT
xc=x_mesh_security+(grid_size/2)
yc=y_mesh_security+(grid_size/2)


#PROCESSING
security_intensity_list=[]
for j in range(len(xc)):
    intensity_row=[]
    for k in range(len(xc[0])):
        kde_value_list=[]
        for i in range(len(x)):
            #CALCULATE DISTANCE
            d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
            if d<=h:
                p=kde_quartic(d,h)
            else:
                p=0
            kde_value_list.append(p)
        #SUM ALL INTENSITY VALUE
        p_total=sum(kde_value_list)
        intensity_row.append(p_total)
    security_intensity_list.append(intensity_row)



 #-------------------FOOD GRADIENT-----------------
#POINT DATASET
x=[bot_right_x]
y=[bot_right_y]

#DEFINE GRID SIZE AND RADIUS(h)
grid_size=1
h=60

#CONSTRUCT GRID
x_grid=np.arange(x_min,x_max,grid_size)
y_grid=np.arange(y_min,y_max,grid_size)
x_mesh_food,y_mesh_food=np.meshgrid(x_grid,y_grid)

#GRID CENTER POINT
xc=x_mesh_food+(grid_size/2)
yc=y_mesh_food+(grid_size/2)


#PROCESSING
food_intensity_list=[]
for j in range(len(xc)):
    intensity_row=[]
    for k in range(len(xc[0])):
        kde_value_list=[]
        for i in range(len(x)):
            #CALCULATE DISTANCE
            d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
            if d<=h:
                p=kde_quartic(d,h)
            else:
                p=0
            kde_value_list.append(p)
        #SUM ALL INTENSITY VALUE
        p_total=sum(kde_value_list)
        intensity_row.append(p_total)
    food_intensity_list.append(intensity_row)




 #-------------------BUILD THE GRADIENTS-----------------

arousal_intensity_array=np.array(arousal_intensity_list)
security_intensity=np.array(security_intensity_list)
food_intensity=np.array(food_intensity_list)






if arousal_circular == False:
    peak_arousal=0
    for i in range(200):
        for j in range(200):
            if arousal_intensity_array[i][j] > peak_arousal:
                peak_arousal = arousal_intensity_array[i][j]

    norm_arousal_intensity = np.linalg.norm(arousal_intensity_array)
    arousal_intensity=arousal_intensity_array/peak_arousal

else:
    arousal_intensity = arousal_intensity_array



if Group_simulations == False: num_simulations = 1
for i in range(num_simulations):

    
    #robot_x=random.randint(10,190)
    #robot_y=random.randint(10,190)

    while timestep<num_episodes:
        timestep += 1
        
        if plots_visualization == True:
            plt.figure(1, figsize=(16,5))

            plot1 = plt.subplot(1,3,1)
            plot1.set_title('AROUSAL')
            plt.plot(robot_x,robot_y,'ro')
            plt.pcolormesh(x_mesh_arousal,y_mesh_arousal,arousal_intensity)

            plot2 = plt.subplot(1,3,2)
            plot2.set_title('SECURITY')
            plt.plot(robot_x,robot_y,'ro')
            plt.pcolormesh(x_mesh_security,y_mesh_security,security_intensity)

            plot3 = plt.subplot(1,3,3)
            plot3.set_title('FOOD')
            plt.plot(robot_x,robot_y,'ro')
            plt.pcolormesh(x_mesh_food,y_mesh_food,food_intensity)

            plt.draw()
            plt.pause(.0001)
            plt.clf()


        Xposition_list.append(robot_x)
        Yposition_list.append(robot_y)


        

        #print("----------AROUSAL----------")
        q0_arousal= (arousal_intensity[robot_y +1, robot_x -1] + arousal_intensity[robot_y +2, robot_x -1] + arousal_intensity[robot_y +3, robot_x -1] + arousal_intensity[robot_y +1, robot_x -2] + arousal_intensity[robot_y +2, robot_x -2] + arousal_intensity[robot_y +3, robot_x -2] + arousal_intensity[robot_y +1, robot_x -3] + arousal_intensity[robot_y +2, robot_x -3] + arousal_intensity[robot_y +3, robot_x -3] + arousal_intensity[robot_y +1, robot_x -4] + arousal_intensity[robot_y +2, robot_x -4] + arousal_intensity[robot_y +3, robot_x -4]) /12
        q1_arousal= (arousal_intensity[robot_y +1, robot_x +1] + arousal_intensity[robot_y +2, robot_x +1] + arousal_intensity[robot_y +3, robot_x +1] + arousal_intensity[robot_y +1, robot_x +2] + arousal_intensity[robot_y +2, robot_x +2] + arousal_intensity[robot_y +3, robot_x +2] + arousal_intensity[robot_y +1, robot_x +3] + arousal_intensity[robot_y +2, robot_x +3] + arousal_intensity[robot_y +3, robot_x +3] + arousal_intensity[robot_y +1, robot_x +4] + arousal_intensity[robot_y +2, robot_x +4] + arousal_intensity[robot_y +3, robot_x +4]) /12
        q2_arousal= (arousal_intensity[robot_y -1, robot_x -1] + arousal_intensity[robot_y -2, robot_x -1] + arousal_intensity[robot_y -3, robot_x -1] + arousal_intensity[robot_y -1, robot_x -2] + arousal_intensity[robot_y -2, robot_x -2] + arousal_intensity[robot_y -3, robot_x -2] + arousal_intensity[robot_y -1, robot_x -3] + arousal_intensity[robot_y -2, robot_x -3] + arousal_intensity[robot_y -3, robot_x -3] + arousal_intensity[robot_y -1, robot_x -4] + arousal_intensity[robot_y -2, robot_x -4] + arousal_intensity[robot_y -3, robot_x -4]) /12
        q3_arousal= (arousal_intensity[robot_y -1, robot_x +1] + arousal_intensity[robot_y -2, robot_x +1] + arousal_intensity[robot_y -3, robot_x +1] + arousal_intensity[robot_y -1, robot_x +2] + arousal_intensity[robot_y -2, robot_x +2] + arousal_intensity[robot_y -3, robot_x +2] + arousal_intensity[robot_y -1, robot_x +3] + arousal_intensity[robot_y -2, robot_x +3] + arousal_intensity[robot_y -3, robot_x +3] + arousal_intensity[robot_y -1, robot_x +4] + arousal_intensity[robot_y -2, robot_x +4] + arousal_intensity[robot_y -3, robot_x +4]) /12


        #print("----------SECURITY----------")
        q0_security= (security_intensity[robot_y +1, robot_x -1] + security_intensity[robot_y +2, robot_x -1] + security_intensity[robot_y +3, robot_x -1] + security_intensity[robot_y +1, robot_x -2] + security_intensity[robot_y +2, robot_x -2] + security_intensity[robot_y +3, robot_x -2] + security_intensity[robot_y +1, robot_x -3] + security_intensity[robot_y +2, robot_x -3] + security_intensity[robot_y +3, robot_x -3] + security_intensity[robot_y +1, robot_x -4] + security_intensity[robot_y +2, robot_x -4] + security_intensity[robot_y +3, robot_x -4]) /12
        q1_security= (security_intensity[robot_y +1, robot_x +1] + security_intensity[robot_y +2, robot_x +1] + security_intensity[robot_y +3, robot_x +1] + security_intensity[robot_y +1, robot_x +2] + security_intensity[robot_y +2, robot_x +2] + security_intensity[robot_y +3, robot_x +2] + security_intensity[robot_y +1, robot_x +3] + security_intensity[robot_y +2, robot_x +3] + security_intensity[robot_y +3, robot_x +3] + security_intensity[robot_y +1, robot_x +4] + security_intensity[robot_y +2, robot_x +4] + security_intensity[robot_y +3, robot_x +4]) /12
        q2_security= (security_intensity[robot_y -1, robot_x -1] + security_intensity[robot_y -2, robot_x -1] + security_intensity[robot_y -3, robot_x -1] + security_intensity[robot_y -1, robot_x -2] + security_intensity[robot_y -2, robot_x -2] + security_intensity[robot_y -3, robot_x -2] + security_intensity[robot_y -1, robot_x -3] + security_intensity[robot_y -2, robot_x -3] + security_intensity[robot_y -3, robot_x -3] + security_intensity[robot_y -1, robot_x -4] + security_intensity[robot_y -2, robot_x -4] + security_intensity[robot_y -3, robot_x -4]) /12
        q3_security= (security_intensity[robot_y -1, robot_x +1] + security_intensity[robot_y -2, robot_x +1] + security_intensity[robot_y -3, robot_x +1] + security_intensity[robot_y -1, robot_x +2] + security_intensity[robot_y -2, robot_x +2] + security_intensity[robot_y -3, robot_x +2] + security_intensity[robot_y -1, robot_x +3] + security_intensity[robot_y -2, robot_x +3] + security_intensity[robot_y -3, robot_x +3] + security_intensity[robot_y -1, robot_x +4] + security_intensity[robot_y -2, robot_x +4] + security_intensity[robot_y -3, robot_x +4]) /12

        #print("----------FOOD----------")
        q0_food= (food_intensity[robot_y +1, robot_x -1] + food_intensity[robot_y +2, robot_x -1] + food_intensity[robot_y +3, robot_x -1] + food_intensity[robot_y +1, robot_x -2] + food_intensity[robot_y +2, robot_x -2] + food_intensity[robot_y +3, robot_x -2] + food_intensity[robot_y +1, robot_x -3] + food_intensity[robot_y +2, robot_x -3] + food_intensity[robot_y +3, robot_x -3] + food_intensity[robot_y +1, robot_x -4] + food_intensity[robot_y +2, robot_x -4] + food_intensity[robot_y +3, robot_x -4]) /12
        q1_food= (food_intensity[robot_y +1, robot_x +1] + food_intensity[robot_y +2, robot_x +1] + food_intensity[robot_y +3, robot_x +1] + food_intensity[robot_y +1, robot_x +2] + food_intensity[robot_y +2, robot_x +2] + food_intensity[robot_y +3, robot_x +2] + food_intensity[robot_y +1, robot_x +3] + food_intensity[robot_y +2, robot_x +3] + food_intensity[robot_y +3, robot_x +3] + food_intensity[robot_y +1, robot_x +4] + food_intensity[robot_y +2, robot_x +4] + food_intensity[robot_y +3, robot_x +4]) /12
        q2_food= (food_intensity[robot_y -1, robot_x -1] + food_intensity[robot_y -2, robot_x -1] + food_intensity[robot_y -3, robot_x -1] + food_intensity[robot_y -1, robot_x -2] + food_intensity[robot_y -2, robot_x -2] + food_intensity[robot_y -3, robot_x -2] + food_intensity[robot_y -1, robot_x -3] + food_intensity[robot_y -2, robot_x -3] + food_intensity[robot_y -3, robot_x -3] + food_intensity[robot_y -1, robot_x -4] + food_intensity[robot_y -2, robot_x -4] + food_intensity[robot_y -3, robot_x -4]) /12
        q3_food= (food_intensity[robot_y -1, robot_x +1] + food_intensity[robot_y -2, robot_x +1] + food_intensity[robot_y -3, robot_x +1] + food_intensity[robot_y -1, robot_x +2] + food_intensity[robot_y -2, robot_x +2] + food_intensity[robot_y -3, robot_x +2] + food_intensity[robot_y -1, robot_x +3] + food_intensity[robot_y -2, robot_x +3] + food_intensity[robot_y -3, robot_x +3] + food_intensity[robot_y -1, robot_x +4] + food_intensity[robot_y -2, robot_x +4] + food_intensity[robot_y -3, robot_x +4]) /12


        aV_arousal = (q0_arousal + q1_arousal + q2_arousal + q3_arousal) / 4
        aVarousal_list.append(aV_arousal)
        dVarousal_list.append(dV_arousal)

        aV_security = (q0_security + q1_security + q2_security + q3_security) / 4
        aVsecurity_list.append(aV_security)
        dVsecurity_list.append(dV_security)

        aV_food = (q0_food + q1_food + q2_food + q3_food) / 4

        aVfood_list.append(aV_food)
        dVfood_list.append(dV_food)




    #-----------------PROBABILISTIC SWITCHING MECHANISM----------------

        if abs(aV_arousal - dV_arousal) < th_switcher_arousal and dV_arousal <= arousal_low_DV:
            if decision(arousal_p_max) == True:
                dV_arousal = arousal_high_DV

        if abs(aV_arousal - dV_arousal) < th_switcher_arousal and dV_arousal >= arousal_high_DV:
            if decision(arousal_p_min) == True:
                dV_arousal = arousal_low_DV



        if abs(aV_security - dV_security) < th_switcher_security and dV_security <= security_low_DV:
            if decision(security_p_max) == True:
                dV_security = security_high_DV

        if abs(aV_security - dV_security) < th_switcher_security and dV_security >= security_high_DV:
            if decision(security_p_min) == True:
                dV_security = security_low_DV



        if abs(aV_food - dV_food) < th_switcher_food and dV_food <= food_low_DV:
            if decision(food_p_max) == True:
                dV_food = food_high_DV

        if abs(aV_food - dV_food) < th_switcher_food and dV_food >= food_high_DV:
            if decision(food_p_min) == True:
                dV_food = food_low_DV



        
        # Equation 2: Defining hsign
        th_arousal = 0
        th_security = 0
        th_food = 0

        if robot_z <= math.pi/8 and robot_z > (math.pi/8) * -1:
            hsign_arousal = np.sign((q0_arousal - q2_arousal)-th_arousal)
            hsign_security = np.sign((q0_security - q2_security)-th_security)
            hsign_food = np.sign((q0_food - q2_food)-th_food)
        elif robot_z >= math.pi/8 and robot_z < (math.pi/8) * 3:
            hsign_arousal = np.sign(((q0_arousal+q2_arousal)/2) - ((q2_arousal+q3_arousal)/2)-th_arousal)
            hsign_security = np.sign(((q0_security+q2_security)/2) - ((q2_security+q3_security)/2)-th_security)
            hsign_food = np.sign(((q0_food+q2_food)/2) - ((q2_food+q3_food)/2)-th_food)
        elif robot_z >= (math.pi/8) * 3 and robot_z < (math.pi/8) * 5:
            hsign_arousal = np.sign((q2_arousal - q3_arousal)-th_arousal)
            hsign_security = np.sign((q2_security - q3_security)-th_security)
            hsign_food = np.sign((q2_food - q3_food)-th_food)
        elif robot_z >= (math.pi/8) * 5 and robot_z < (math.pi/8) * 7:
            hsign_arousal = np.sign(((q2_arousal+q3_arousal)/2) - ((q3_arousal+q1_arousal)/2)-th_arousal)
            hsign_security = np.sign(((q2_security+q3_security)/2) - ((q3_security+q1_security)/2)-th_security)
            hsign_food = np.sign(((q2_food+q3_food)/2) - ((q3_food+q1_food)/2)-th_food)
        elif robot_z >= (math.pi/8) * 7 or robot_z < (math.pi/8) * (-7):
            hsign_arousal = np.sign((q3_arousal - q1_arousal)-th_arousal)
            hsign_security = np.sign((q3_security - q1_security)-th_security)
            hsign_food = np.sign((q3_food - q1_food)-th_food)
        elif robot_z >=  (math.pi/8) * (-7) and robot_z < (math.pi/8) * (-5):
            hsign_arousal = np.sign(((q3_arousal+q1_arousal)/2) - ((q1_arousal+q0_arousal)/2)-th_arousal)
            hsign_security = np.sign(((q3_security+q1_security)/2) - ((q1_security+q0_security)/2)-th_security)
            hsign_food = np.sign(((q3_food+q1_food)/2) - ((q1_food+q0_food)/2)-th_food)
        elif robot_z >= (math.pi/8) * (-5) and robot_z < (math.pi/8) * (-3):
            hsign_arousal = np.sign((q1_arousal - q0_arousal)-th_arousal)
            hsign_security = np.sign((q1_security - q0_security)-th_security)
            hsign_food = np.sign((q1_food - q0_food)-th_food)
        elif robot_z >= (math.pi/8) * (-3) and robot_z < (math.pi/8) * (-1):
            hsign_arousal = np.sign(((q1_arousal+q0_arousal)/2) - ((q0_arousal+q2_arousal)/2)-th_arousal)
            hsign_security = np.sign(((q1_security+q0_security)/2) - ((q0_security+q2_security)/2)-th_security)
            hsign_food = np.sign(((q1_food+q0_food)/2) - ((q0_food+q2_food)/2)-th_food)
        '''else:
                        print("SOMETHING VERY WEIRD HAPPENED-------------------")'''

        


        ADsign_arousal = np.sign((dV_arousal - aV_arousal) - th_arousal)    
        total_force_arousal = weighting_factor_arousal * abs(aV_arousal - dV_arousal) #f

        ADsign_security = np.sign((dV_security - aV_security) - th_security)
        total_force_security = weighting_factor_security * abs(aV_security - dV_security) #f

        ADsign_food = np.sign((dV_food - aV_food) - th_food)
        total_force_food = weighting_factor_food * abs(aV_food - dV_food) #f


        
        
        left_wheel = -1 * ((hsign_arousal * ADsign_arousal* total_force_arousal) + (hsign_security * ADsign_security* total_force_security) + (hsign_food * ADsign_food* total_force_food)) * (1/3)
        #print("LEFT WHEEL: " + str(left_wheel))



        #---------- Z VARIABILITY ------------ 
        z_variability = random.gauss(mu, sigma)


        #----------VECTOR ORIENTATION---------

        if robot_z + left_wheel > math.pi:
            robot_z = (math.pi * -1) + left_wheel
        elif robot_z + left_wheel < math.pi * -1:
            robot_z = math.pi + left_wheel
        else:
            robot_z += left_wheel + z_variability




        #-----------MOTOR MAPPING-----------

        #GETTING X,Y MIN AND MAX
        x_min_matrix = x_min + 5
        y_min_matrix = y_min + 5
        x_max_matrix = x_max - 5
        y_max_matrix = y_max - 5


        if robot_z <= math.pi/8 and robot_z > (math.pi/8) * -1:
            if robot_x > x_min_matrix:
                robot_x -= 1
            else:
                if robot_z >= 0:
                    robot_z = math.pi /2 # Down
                else:
                    robot_z = (-1*math.pi) /2 # Up
            #print("LEFT")
        elif robot_z >= math.pi/8 and robot_z < (math.pi/8) * 3:
            if robot_x > x_min_matrix and robot_y > y_min_matrix: 
                robot_x -= 1
                robot_y -= 1
            else:
                if robot_x <= x_min_matrix and robot_y > y_min_matrix:
                    robot_z = math.pi /2 # Down
                if robot_x <= x_min_matrix and robot_y <= y_min_matrix:
                    if robot_x > math.pi/4:
                        robot_z = (-1*math.pi) /2 # Up
                    else:
                        robot_z = math.pi # Right
                if robot_x <= x_min_matrix and robot_y <= y_min_matrix:
                    robot_z = 0 # Left

            #print("LEFT-DOWN")
        elif robot_z >= (math.pi/8) * 3 and robot_z < (math.pi/8) * 5:
            if robot_y > y_min_matrix:
                robot_y -= 1
            else:
                if robot_z >= (math.pi/2):
                    robot_z = math.pi # RIGHT
                else:
                    robot_z = 0 # LEFT
            #print("DOWN")
        elif robot_z >= (math.pi/8) * 5 and robot_z < (math.pi/8) * 7:
            if robot_y > y_min_matrix:
                robot_y -= 1
            if robot_x < x_max_matrix:
                robot_x += 1
            else:
                if robot_z >= math.pi - (math.pi/4):
                    robot_z = (-1*math.pi) /2 # UP
                else:
                    robot_z = 0 # LEFT
            #print("RIGHT-DOWN")
        elif robot_z >= (math.pi/8) * 7 or robot_z < (math.pi/8) * (-7):
            #if robot_z > math.pi or robot_z < (math.pi * -1):
                #print("------------CAUTION robot z = " + str(robot_z))
            if robot_x < x_max_matrix:
                robot_x += 1
            else:
                if robot_z >= 0:
                    robot_z = math.pi /2 # DOWN
                else:
                    robot_z = (-1*math.pi) /2 # UP
            #print("RIGHT")
        elif robot_z >=  (math.pi/8) * (-7) and robot_z < (math.pi/8) * (-5):
            if robot_x < x_max_matrix:
                robot_x += 1
            if robot_y < y_max_matrix:
                robot_y += 1
            else:
                if robot_z >= (-1*math.pi) + (math.pi/4):
                    robot_z = 0 # LEFT
                else:
                    robot_z = math.pi /2 # DOWN
            #print("RIGHT-UP")
        elif robot_z >= (math.pi/8) * (-5) and robot_z < (math.pi/8) * (-3):
            if robot_y < y_max_matrix:
                robot_y += 1
            else:
                if robot_z >= (math.pi/2) * -1:
                    robot_z = 0 # LEFT
                else:
                    robot_z = math.pi # RIGHT
            #print("UP")
        elif robot_z >= (math.pi/8) * (-3) and robot_z < (math.pi/8) * (-1):
            if robot_x > x_min_matrix:
                robot_x -= 1
            if robot_y < y_max_matrix:
                robot_y += 1
            else:
                if robot_z >= (-1*math.pi) /4:
                    robot_z = math.pi /2 # DOWN
                else:
                    robot_z = math.pi # RIGHT
            #print("LEFT-UP")



        csv_namefile = '/home/roboticslab/Robotology/Repos/allostractor/data/2010Replication/3gradients/' + str(i+1) + '.csv'
    print(csv_namefile)

    timestep=0

    with open(csv_namefile, mode='w') as csv_file:

        csv_writer = csv.DictWriter(csv_file, fieldnames=['Xposition', 'Yposition', 'aVarousal', 'dVarousal', 'aVsecurity', 'dVsecurity', 'aVfood', 'dVfood'])
        csv_writer.writeheader()

        for i in range (len(Xposition_list)):
            csv_writer.writerow({'Xposition': Xposition_list[i], 'Yposition': Yposition_list[i], 'aVarousal': aVarousal_list[i], 'dVarousal': dVarousal_list[i], 'aVsecurity': aVsecurity_list[i], 'dVsecurity': dVsecurity_list[i], 'aVfood': aVfood_list[i], 'dVfood': dVfood_list[i]})

    Xposition_list = []
    Yposition_list = []
    aVarousal_list = []
    dVarousal_list = []
    aVsecurity_list = []
    dVsecurity_list = []
    aVfood_list = []
    dVfood_list = []

