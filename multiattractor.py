import numpy as np
from scipy.integrate import odeint


class MultiAttractor(object):
	
	def __init__(self):
		'''
		PARAMETERS
		'''
		self.dt = 0.001
		# number of populations
		self.N = 9

		# couplings
		self.we = np.ones(self.N)*5
		self.wsi = 4
		self.wmi = 4

		self.q = 0.2  # ACh level (0 corresponds to shared inhibition and 1 to mutual inhibition)

		# external inputs
		self.I = np.ones(self.N)*(-100)

		# Initial state
		self.U = np.zeros(self.N)

		# Initial 'dummy' variance
		self.var = 15

		#time decay constants
		self.tau = 0.02

		# activation function
		self.a = 1/22
		self.thr = 15
		self.fmax = 40
		

	def sigmoid(self, x):
		return self.fmax / (1 + np.exp(-self.a * (x - self.thr)))

	# this function returns the right hand side of the modified Wilson-Cowan equation
	def WilsonCowan(self, I):

		y = ( -self.U + self.sigmoid(self.we * self.U - self.q * self.wmi * (np.dot(np.abs(np.identity(self.N)-1), self.U)) - (1-self.q) * self.wsi * self.sigmoid(np.sum(self.U)) + I) + np.random.normal(0,1, self.N)*self.var )/self.tau
		
		return y

	def advance(self, I, time):

		for t in np.arange(time):
			
			y = self.WilsonCowan(I)
			self.U += y*self.dt
			
			# ReLU correction
			self.U *= self.U>0

		return self.U

	def reset(self):

		self.U = np.zeros(self.N)
